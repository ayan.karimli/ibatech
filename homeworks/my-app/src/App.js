import React, {Component} from 'react';
import Card from "./components/Card/Card.js";
import Modal from "./components/Modal/Modal.js";
import TopOfPage from "./components/TopOfPage/TopOfPage.js";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            showModal: false,
            selectedCards: [],
        };
    }
    ;
    showCard = (e) => {

        const selectedCard = this.state.products.filter((card) => parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        this.state.selectedCards.push(selectedCard);
        this.setState({showModal: true, selectedCards: this.state.selectedCards});
        localStorage.setItem("Cards", JSON.stringify(this.state.selectedCards));
    };

    addFavorites = (e) => {
        const favCard = this.state.products.filter((card) => parseInt(e.target.parentElement.parentElement.getAttribute('id')) === card.id);
        if (!e.target.classList.contains("checked")) {
            e.target.className += " checked";
            this.state.favCards.push(favCard);
            this.setState({favCards: this.state.selectedCards});
            console.log(this.state.favCards);
            localStorage.setItem("Favorites", JSON.stringify(this.state.favCards));
        } else {
            e.target.classList.remove("checked");
        }
    };
    hideCard = () => {
        this.setState({showModal: false});
    };

    hideCardOutside = (e) => {
        if (this.state.showModal) {
            this.setState({showModal: false});
        }
    };

    componentDidMount() {

        fetch("/data.json").then(r => r.json())
            .then(data => {
                this.setState({
                    products: data
                });
            });
    }

    render() {
        const cards = this.state.products.map((item) => {
            return <Card product={item} key={item.id} openCard={this.showCard} handleFav={this.addFavorites}/>;
        });


        return (
            <>
                <TopOfPage hideCard={this.hideCard}/>
                <div className={"container"} onClick={this.hideCardOutside}>

                    {

                        this.state.products.length > 0 ?
                            cards
                            : "loading"
                    }
                </div>
                {
                    this.state.showModal ?
                        <div className={"modal-card"}>

                            {
                                <Modal hideCard={this.hideCard}/>
                            }
                        </div> : null
                }
            </>
        );
    }
}

export default App;
