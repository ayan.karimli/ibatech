import React, {Component} from 'react';
import "../TopOfPage/style.scss";

class Card extends Component {
    addToCard=(e)=>{

    };
    render() {

        return (
            <>
                <div className={"card"} id={this.props.product.id}>
                    <img src={this.props.product.path} alt=""/>
                    <div className={"text-container"}>
                        <span><strong>{this.props.product.name}</strong> <i> by Artist</i></span>
                        <p className={"text"}>Lorem ipsum dolor sit amet, con
                            adipiscing elit, sed diam nonu. </p>
                        <strong className={"price"}>${this.props.product.price}</strong>
                        <div onClick={this.props.openCard }  className={"button"}>ADD TO CARD</div>
                    </div>
                </div>
            </>

        );
    }
}

export default Card;
