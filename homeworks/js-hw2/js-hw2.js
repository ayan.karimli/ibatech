let num1 = parseInt(prompt('Enter the first number', '5'));

if (isNaN(num1)){
    alert('Please enter the correct number');
}
let num2 = parseInt(prompt('Enter the second number', '6'));

if (isNaN(num2)){
    alert('Please enter the correct number');
}

let operation = prompt('Enter the operation', '+');

function sum(a, b) {
return a+b;
}

function sub(a, b) {
return a-b;
}

function mul(a, b) {
return a*b;
}

function div(a, b) {
return a/b;
}

function calc(num1 , num2, operation) {
    let result =0;
    if (operation === '+') {
        result = sum(num1, num2);
    }
   else if (operation === '-') {
        result = sub(num1, num2);
    }
   else if (operation === '*') {
        result = mul(num1, num2);
    }
   else if (operation === '/') {
        result = div(num1, num2);
    }
   return result;
}

let result = calc(num1 , num2, operation);
console.log(result);

