const _CATEGORIES = Object.freeze([
    'Web Design',
    'Graphic Design',
    'Landing Pages',
    'Wordpress'

]);

// const img_gallery = document.querySelector('gallery');
// document.getElementById('img-gallery');
const _TABS_CONTENT = Object.freeze([
    {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design1.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design2.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design3.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design4.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design5.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design6.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design7.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design8.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design9.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design10.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design11.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/graphic_design/graphic-design12.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },

    {
        imgSrc: 'StepProjectHam/landing_page/landing-page1.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/landing_page/landing-page2.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/landing_page/landing-page3.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/landing_page/landing-page4.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/landing_page/landing-page5.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/landing_page/landing-page6.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/landing_page/landing-page7.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },


    {
        imgSrc: 'StepProjectHam/web_design/web-design1.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/web_design/web-design2.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/web_design/web-design3.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/web_design/web-design4.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/web_design/web-design5.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/web_design/web-design6.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/web_design/web-design7.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },

    {
        imgSrc: 'StepProjectHam/wordpress/wordpress1.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress2.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress3.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress4.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress5.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress6.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress7.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress8.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    }, {
        imgSrc: 'StepProjectHam/wordpress/wordpress9.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },
    {
        imgSrc: 'StepProjectHam/wordpress/wordpress10.jpg',
        title: 'CREATIVE DESIGN',
        category: _CATEGORIES[Math.floor(Math.random() * (_CATEGORIES.length))]
    },

]);


_TABS_CONTENT.forEach(tabItem => {
    const {imgSrc, title, category} = tabItem,
        container = document.createElement('div'),
        img = document.createElement('img');

    container.classList.add('gallery-item');
    img.classList.add('gallery-item-img');

    img.src = imgSrc;
    container.dataset.category = category;
    console.log(category);
    container.addEventListener('mouseenter', e => {
        const hoverContainer = document.createElement('div'),
            hoverIconShare = document.createElement('img'),
            hoverIconSearch = document.createElement('img'),
            hoverTitle = document.createElement('h4'),
            hoverCategory = document.createElement('p');

        hoverContainer.classList.add('gallery-hover-wrapper');
        hoverIconShare.classList.add('gallery-hover-img');
        hoverIconSearch.classList.add('gallery-hover-img');
        hoverTitle.classList.add('gallery-hover-title');
        hoverCategory.classList.add('gallery-hover-category');

        hoverIconSearch.src = 'options/search-icon.png';
        hoverIconShare.src = 'options/share-icon.png';
        hoverTitle.innerText = title;
        hoverCategory.innerText = category;

        hoverContainer.append(
            hoverIconShare,
            hoverIconSearch,
            hoverTitle,
            hoverCategory
        );

        container.append(hoverContainer);
    });
    container.addEventListener('mouseleave', e => {
        container.querySelector('.gallery-hover-wrapper').remove();
    });

    container.append(
        img
    );

    document.querySelector('.gallery-wrapper').append(container)
});

let loadMoreBtn = document.createElement('button');
 let loadMorePlus = document.createElement("span");
let plus = document.createElement('span');
plus.innerText = "+";
plus.classList.add("load-more-plus");
loadMorePlus.innerText = "+";
loadMorePlus.classList.add("load-more-plus");
 loadMoreBtn.classList.add("load-more");
 loadMoreBtn.innerText = "LOAD MORE";
loadMoreBtn.appendChild(plus);

loadMoreBtn.addEventListener('click',()=>{
 let x = document.querySelector('.load-more');
    let images = document.querySelectorAll('.gallery-item');
    images.forEach((z =>{
        z.style.display="block";
    }));

});

document.querySelector('.gallery').appendChild(loadMoreBtn);


const amazingWorkTabs = document.querySelectorAll('.our-amazing-work-menu-item');
amazingWorkTabs.forEach(tab => {

    tab.addEventListener('click', function (event) {
        let tabsImages = document.querySelectorAll(".gallery-item");

        console.log(tabsImages);

        tabsImages.forEach((tabsImg => {
            tabsImg.style.display = "none";

        }));

        tabsImages.forEach(img=>{
            if (event.target.innerText === img.dataset.category){
                img.style.display = "block"
            }
        });
amazingWorkTabs.forEach((tab)=>{
    tab.classList.remove("our-amazing-work-menu-item-active")
    }
);
        tab.classList.add("our-amazing-work-menu-item-active")
    })
});

// console.log(Math.floor());

amazingWorkTabs[0].addEventListener("click",(event)=>{
    let tabsImages = document.querySelectorAll(".gallery-item");
tabsImages.forEach(img=>{
    img.style.display = 'block'
})
});



//our services event

let ourServicesTabs = document.querySelectorAll('.our-services-menu-item');
let tabsContent = document.querySelectorAll('.tabs-content');
for (let i=0;i<ourServicesTabs.length;i++){
    ourServicesTabs[i].addEventListener("click", (event)=>{

        for (let j=0;j<ourServicesTabs.length;j++){
            ourServicesTabs[j].classList.remove("our-services-menu-item-active");
tabsContent[j].classList.remove('visible');
        }
        ourServicesTabs[i].classList.add("our-services-menu-item-active");
        tabsContent[i].classList.add('visible')
    })
}