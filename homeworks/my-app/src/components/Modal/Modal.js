import React, {Component} from 'react';

class Modal extends Component {

    render() {
        return (

            <div className={"modal-text"}>
                <p>You succesfully added this item into your basket. Please, continue..</p>
                <button className={"btn"} onClick={this.props.hideCard}>OK</button>
                <button className={"btn"} onClick={this.props.hideCard}>Cancel</button>
            </div>
        );
    }
}

export default Modal;
